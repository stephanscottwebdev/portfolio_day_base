<!DOCTYPE html>
<html>
<head>
<!-- CSS
---------------------------------->
<link rel="stylesheet" href="foundation/css/foundation.css" />
<link rel="stylesheet" href="scss.php/style.scss" />
<!-- JS
---------------------------------->
<script src="foundation/js/vendor/modernizr.js"></script>

<!-- Meta
---------------------------------->
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width">
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

<title>Index</title>
</head>
<body>
	<header>
		<nav>

		</nav>
	</header>
	<section id="top">
		<div class="outer">
			<div class="middle">
				<div class="inner">
					<div class="row text-center center">
						<div class="large-12 columns">
							<div class="planet-1-circle planet-1 center">
								<div class="circle-inner"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>	
	
	<section id="middle">
		<!-- Content here -->
	</section>	

	<section id="bottom">
		<!-- Content here -->
	</section>	
	
	<footer id="footer" class="large-12 columns">
		<p>Footer</p>
	</footer>
	
<!-- JS ---------------------------------->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="foundation/js/foundation.min.js"></script>
	<script src="js/script.js"></script>
	<script>
		$(document).foundation();
	</script>
</body>
</html>